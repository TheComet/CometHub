package co.thecomet.hub.modules.parkour;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.config.JsonLocation;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.hub.HubModule;
import co.thecomet.hub.db.HubDataAPI;
import co.thecomet.hub.db.models.PlayerHubData;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class CheckpointListener implements Listener {
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null) {
            return;
        }

        Block b = event.getClickedBlock();
        if(b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN_POST) {
            Sign sign = (Sign) b.getState();
            if (validate(sign.getLines(), event.getClickedBlock().getLocation())) {
                if (sign.getLine(0).equals("[CHECKPOINT]") == false) {
                    sign.setLine(0, "[CHECKPOINT]");
                    sign.update();
                }

                if (CoreAPI.isPlayerLoaded(event.getPlayer())) {
                    Checkpoint checkpoint = ParkourModule.getInstance().getCheckpoint(sign.getLocation());
                    PlayerHubData data = HubModule.getInstance().getPlayerHubData(event.getPlayer().getUniqueId());

                    if (data.checkpoint == null || data.checkpoint.equals(checkpoint.getSignLocation()) == false) {
                        data.checkpoint = checkpoint.getSignLocation();
                        HubDataAPI.updateCheckpoint(event.getPlayer().getUniqueId(), data);
                        MessageFormatter.sendSuccessMessage(event.getPlayer(), "You have set your check point.");
                    }
                }
            }
        }
    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        NetworkPlayer player;
        if ((player = CoreAPI.getPlayer(event.getPlayer().getUniqueId())) != null && player.getRank().ordinal() >= Rank.ADMIN.ordinal()) {
            validate(event.getLines(), event.getBlock().getLocation());
        } else {
            MessageFormatter.sendErrorMessage(event.getPlayer(), "You cannot create checkpoint signs.");
            event.getBlock().setType(Material.AIR);
        }
    }

    private boolean validate(String[] lines, Location location) {
        if (lines[0].equalsIgnoreCase("checkpoint") || lines[0].equalsIgnoreCase("[CHECKPOINT]")) {
            if (ParkourModule.getInstance().containsCheckpoints(location) == false) {
                ParkourModule.getInstance().addCheckpoint(location, new Checkpoint(new JsonLocation(location)));
            }

            return true;
        }
        
        return false;
    }
}
