package co.thecomet.hub.modules.parkour;

import co.thecomet.core.config.JsonLocation;

public class Checkpoint {
    private JsonLocation signLocation;

    public Checkpoint(JsonLocation signLocation) {
        this.signLocation = signLocation;
    }

    public JsonLocation getSignLocation() {
        return signLocation;
    }
}
