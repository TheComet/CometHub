package co.thecomet.hub.modules.cosmetic;

import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.ArrayList;
import java.util.List;

public abstract class UnlockableMenu extends Menu {
    private static final Multimap<UnlockableMenu, Unlockable> unlockables = HashMultimap.create();

    public UnlockableMenu(String title, int rows) {
        super(title, rows);
    }

    public static List<Unlockable> getUnlockables() {
        return new ArrayList<>(unlockables.values());
    }

    public static List<Unlockable> getUnlockables(UnlockableMenu menu) {
        return new ArrayList<>(unlockables.get(menu));
    }

    public static void registerUnlockable(UnlockableMenu menu, Unlockable unlockable) {
        unlockables.put(menu, unlockable);
    }

    public static void unregisterUnlockables() {
        unlockables.clear();
    }

    public static void unregisterUnlockables(UnlockableMenu menu) {
        unlockables.removeAll(menu);
    }

    public abstract MenuItem getItem();
}
