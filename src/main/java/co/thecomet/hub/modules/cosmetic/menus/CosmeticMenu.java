package co.thecomet.hub.modules.cosmetic.menus;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.hub.modules.cosmetic.CosmeticsModule;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import co.thecomet.hub.modules.cosmetic.UnlockableMenu;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.List;

public class CosmeticMenu extends Menu {
    private final List<UnlockableMenu> menus = new ArrayList<>();
    private final MenuItem item = new MenuItem(FontColor.translateString("&6&lC&eosmetic"), new MaterialData(Material.CHEST)) {
        @Override
        public void onClick(Player player) {
            if (getInventory().getViewers().contains(player) == false) {
                openMenu(player);
            }
        }
    };
    private CosmeticsModule module;

    public CosmeticMenu(CosmeticsModule module) {
        super(FontColor.translateString("&6&lC&eosmetic"), 1);
        this.item.setSlot(4);
        this.module = module;
        
        init();
    }
    
    public void init() {
        menus.add(new GadgetsMenu(this));
        menus.add(new CostumesMenu(this));
        menus.add(new ParticlesMenu(this));
        if (Bukkit.getPluginManager().isPluginEnabled("EchoPet")) {
            menus.add(new PetMenu(this));
        }

        for (UnlockableMenu menu : menus) {
            MenuItem i = menu.getItem();
            if (i != null) {
                if (i.getSlot() >= 0 && i.getSlot() < 9) {
                    this.addMenuItem(i, i.getSlot());
                }
            }
        }
    }

    public List<Listener> getListeners() {
        List<Listener> listeners = new ArrayList<>();
        for (Unlockable unlockable : UnlockableMenu.getUnlockables()) {
            if (unlockable instanceof Listener) {
                listeners.add((Listener) unlockable);
            }
        }
        return listeners;
    }

    public MenuItem getItem() {
        return item;
    }

    public CosmeticsModule getModule() {
        return module;
    }
}
