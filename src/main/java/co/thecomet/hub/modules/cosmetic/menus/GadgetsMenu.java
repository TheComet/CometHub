package co.thecomet.hub.modules.cosmetic.menus;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import co.thecomet.hub.modules.cosmetic.UnlockableMenu;
import co.thecomet.hub.modules.cosmetic.unlockables.gadgets.FireworkHurler;
import co.thecomet.hub.modules.cosmetic.unlockables.gadgets.MobLauncher;
import co.thecomet.hub.modules.cosmetic.unlockables.gadgets.Rainbow;
import co.thecomet.hub.modules.cosmetic.unlockables.gadgets.RainbowRunner;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class GadgetsMenu extends UnlockableMenu {
    protected CosmeticMenu mainMenu;
    protected final MenuItem item = new MenuItem(FontColor.translateString("&9Gadgets"), new MaterialData(Material.COMMAND)) {
        @Override
        public void onClick(Player player) {
            if (getInventory().getViewers().contains(player) == false) {
                openMenu(player);
            }
        }
    };
    
    public GadgetsMenu(CosmeticMenu mainMenu) {
        super(FontColor.translateString("&9Gadgets"), 1);
        this.mainMenu = mainMenu;
        this.item.setSlot(1);

        init();
    }

    private void init() {
        registerUnlockable(this, new MobLauncher());
        registerUnlockable(this, new FireworkHurler());
        registerUnlockable(this, new RainbowRunner());
        registerUnlockable(this, new Rainbow());

        for (Unlockable unlockable : UnlockableMenu.getUnlockables(this)) {
            if (unlockable.getMenuSlot() < 0 || unlockable.getMenuSlot() > 53) {
                continue;
            }

            this.addMenuItem(unlockable, unlockable.getMenuSlot());
        }
    }

    public MenuItem getItem() {
        return item;
    }
}
