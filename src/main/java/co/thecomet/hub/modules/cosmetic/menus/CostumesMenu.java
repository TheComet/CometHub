package co.thecomet.hub.modules.cosmetic.menus;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import co.thecomet.hub.modules.cosmetic.UnlockableMenu;
import co.thecomet.hub.modules.cosmetic.unlockables.costumes.ChameleonSuit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class CostumesMenu extends UnlockableMenu {
    protected CosmeticMenu mainMenu;
    protected final MenuItem item = new MenuItem(FontColor.translateString("&aCostumes"), new MaterialData(Material.ARMOR_STAND)) {
        @Override
        public void onClick(Player player) {
            if (getInventory().getViewers().contains(player) == false) {
                openMenu(player);
            }
        }
    };

    public CostumesMenu(CosmeticMenu mainMenu) {
        super(FontColor.translateString("&aCostumes"), 1);
        this.mainMenu = mainMenu;
        this.item.setSlot(5);

        init();
    }

    private void init() {
        registerUnlockable(this, new ChameleonSuit());

        for (Unlockable unlockable : UnlockableMenu.getUnlockables(this)) {
            if (unlockable.getMenuSlot() < 0 || unlockable.getMenuSlot() > 53) {
                continue;
            }

            this.addMenuItem(unlockable, unlockable.getMenuSlot());
        }
    }

    public MenuItem getItem() {
        return item;
    }
}
