package co.thecomet.hub.modules.cosmetic;

import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.hub.modules.cosmetic.menus.CosmeticMenu;
import co.thecomet.hub.modules.toolbar.Toolbar;
import co.thecomet.hub.modules.toolbar.ToolbarModule;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;

@ModuleInfo(name = "vipunlockables", depends = {ToolbarModule.class})
public class CosmeticsModule extends Module {
    protected CosmeticMenu menu;
    
    public void onEnable() {
        menu = new CosmeticMenu(this);
        Toolbar.register(this, menu.getItem());
    }

    public void onDisable() {
        Toolbar.unregister(menu.getItem());
    }

    @Override
    public List<Listener> registerListeners() {
        return new ArrayList<Listener>() {{
            add(new CosmeticListener());
            addAll(menu.getListeners());
        }};
    }
}
