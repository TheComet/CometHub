package co.thecomet.hub.modules.cosmetic;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class CosmeticListener implements Listener {
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Unlockable unlockable = Unlockable.activeUnlockable.get(event.getPlayer().getUniqueId());
        if (unlockable != null) {
            unlockable.deactivate(event.getPlayer());
        }
    }
}
