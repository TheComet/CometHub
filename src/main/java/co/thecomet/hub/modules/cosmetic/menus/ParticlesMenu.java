package co.thecomet.hub.modules.cosmetic.menus;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import co.thecomet.hub.modules.cosmetic.UnlockableMenu;
import co.thecomet.hub.modules.cosmetic.unlockables.particles.*;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class ParticlesMenu extends UnlockableMenu {
    protected CosmeticMenu mainMenu;
    protected final MenuItem item = new MenuItem(FontColor.translateString("&cParticles"), new MaterialData(Material.REDSTONE)) {
        @Override
        public void onClick(Player player) {
            if (getInventory().getViewers().contains(player) == false) {
                openMenu(player);
            }
        }
    };

    public ParticlesMenu(CosmeticMenu mainMenu) {
        super(FontColor.translateString("&cParticles"), 1);
        this.mainMenu = mainMenu;
        this.item.setSlot(3);

        init();
    }

    private void init() {
        registerUnlockable(this, new CupidsHalo());
        registerUnlockable(this, new Orbital());
        registerUnlockable(this, new RainCloud());
        registerUnlockable(this, new BloodHelix());
        registerUnlockable(this, new HellStorm());

        for (Unlockable unlockable : UnlockableMenu.getUnlockables(this)) {
            if (unlockable.getMenuSlot() < 0 || unlockable.getMenuSlot() > 53) {
                continue;
            }

            this.addMenuItem(unlockable, unlockable.getMenuSlot());
        }
    }

    public MenuItem getItem() {
        return item;
    }
}
