package co.thecomet.hub.modules.cosmetic.unlockables.pets;

import co.thecomet.hub.modules.cosmetic.Unlockable;
import com.dsh105.echopet.compat.api.entity.IPet;
import com.dsh105.echopet.compat.api.entity.PetType;
import com.dsh105.echopet.compat.api.plugin.EchoPet;
import com.dsh105.echopet.compat.api.plugin.IPetManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PetUnlockable extends Unlockable {
    private static final Map<UUID, PetUnlockable> ACTIVE_PET_TYPE = new HashMap<>();
    private final PetType pet;
    private final PetType rider;
    private boolean rideable = true;
    private boolean nameable = true;

    public PetUnlockable(String display, PetType pet, Material material) {
        this(display, pet, null, new MaterialData(material));
    }

    public PetUnlockable(String display, PetType pet, PetType rider, Material material) {
        this(display, pet, rider, new MaterialData(material));
    }

    public PetUnlockable(String display, PetType pet, Material material, byte data) {
        this(display, pet, null, material, data);
    }

    public PetUnlockable(String display, PetType pet, PetType rider, Material material, byte data) {
        this(display, pet, rider, new MaterialData(material, data));
    }

    public PetUnlockable(String display, PetType pet, PetType rider, MaterialData data) {
        super(display, data);
        this.pet = pet;
        this.rider = rider;
    }

    @Override
    protected void onActivate(Player player) {
        ACTIVE_PET_TYPE.put(player.getUniqueId(), this);
        create(player);
    }

    @Override
    protected void onDeactivate(Player player) {
        ACTIVE_PET_TYPE.remove(player.getUniqueId());
        destroy(player);
    }

    public PetUnlockable setRideable(boolean rideable) {
        this.rideable = rideable;
        return this;
    }

    public PetUnlockable setNameable(boolean nameable) {
        this.nameable = nameable;
        return this;
    }

    public void create(Player player) {
        IPet pet = null;
        if (rider != null) {
            pet = getPetManager().createPet(player, this.pet, rider);
        } else {
            pet = getPetManager().createPet(player, this.pet, false);
        }

        if (pet == null) {
            deactivate(player);
            return;
        }
    }

    public void destroy(Player player) {
        IPet pet = getPetManager().getPet(player);
        if (pet != null) {
            pet.removeRider();
            pet.removePet(false);
        }
    }

    public static IPetManager getPetManager() {
        return EchoPet.getManager();
    }
}
