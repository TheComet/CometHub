package co.thecomet.hub.modules.scoreboard;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.hub.HubModule;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

@ModuleInfo(name = "scoreboard", depends = {HubModule.class})
public class ScoreboardModule extends Module {
    private ScoreboardManager scoreboardManager;
    private BukkitTask task;

    @Override
    public void onEnable() {
        task = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), scoreboardManager = new ScoreboardManager(), 0, 10);
    }

    @Override
    public void onDisable() {
        task.cancel();
    }

    @Override
    public List<Listener> registerListeners() {
        return new ArrayList<Listener>(){{
            add(scoreboardManager);
        }};
    }
}
