package co.thecomet.hub.modules.gamemenu;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.hub.status.ServerType;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GameMenu extends Menu {
    private static Map<ServerType, GameListMenu> gameLists = new HashMap<>();

    public GameMenu() {
        super(FontColor.translateString("&2&lG&aames"), 1);
        init(ServerType.SG, 0, 0);
        init(ServerType.SW, 2, 0);
        init(ServerType.WARGROUNDS, 4, 0);
    }

    public void init(ServerType type, int x, int y) {
        GameListMenu menu = new GameListMenu(type);
        create(type, menu, x, y);
    }

    private void create(ServerType type, GameListMenu menu, int x, int y) {
        MenuItem item = new MenuItem(FontColor.translateString(type.getDisplay()), new MaterialData(type.getIcon())) {
            @Override
            public void onClick(Player player) {
                menu.openMenu(player);
            }
        };

        this.addMenuItem(item, x, y);
    }

    public void disable() {
        new ArrayList<>(gameLists.keySet()).forEach((type) -> {
            GameListMenu menu = gameLists.get(type);
            if (menu != null) {
                menu.cancel();
                gameLists.remove(menu);
            }
        });
    }
}
