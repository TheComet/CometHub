package co.thecomet.hub.modules.delivery;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.hub.Hub;
import co.thecomet.hub.modules.delivery.data.DeliveryType;
import co.thecomet.hub.modules.delivery.data.UserDeliveryStats;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.Date;
import java.util.UUID;

public class DeliveryDataAPI {
    public static UserDeliveryStats getDeliveryStats(UUID uuid) {
        UserDeliveryStats stats = Hub.getUserDeliveryStatsDAO().findOne("uuid", uuid.toString());

        if (stats == null) {
            stats = new UserDeliveryStats(uuid);
            Hub.getUserDeliveryStatsDAO().save(stats);
        }

        NetworkPlayer player = CoreAPI.getPlayer(uuid);
        if (player != null) {
            player.setData(UserDeliveryStats.class, stats);
        }

        return stats;
    }

    public static void updateUserDeliveries(UUID uuid, DeliveryType type, Date date) {
        UserDeliveryStats stats = getDeliveryStats(uuid);
        stats.getStats().put(type, date);

        UpdateOperations<UserDeliveryStats> ops = Hub.getUserDeliveryStatsDAO().createUpdateOperations();
        ops.set("stats", stats.getStats());
        Hub.getUserDeliveryStatsDAO().update(Hub.getUserDeliveryStatsDAO().createQuery().field("uuid").equal(uuid.toString()), ops);
    }
}
