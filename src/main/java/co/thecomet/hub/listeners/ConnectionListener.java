package co.thecomet.hub.listeners;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.events.LoginSuccessEvent;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.hub.Hub;
import co.thecomet.hub.db.HubDataAPI;
import co.thecomet.hub.db.models.PlayerHubData;
import co.thecomet.hub.modules.preferences.PreferencesMenu;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;

public class ConnectionListener implements Listener {
    @EventHandler
    public void onLoginSuccess(LoginSuccessEvent event) {
        NetworkPlayer player = event.getProfile();
        PlayerHubData data = HubDataAPI.getPlayerHubData(player.getUuid());

        if (data == null) {
            data = new PlayerHubData(player.getUuid());

            final PlayerHubData finalData = data;
            CoreAPI.async(() -> Hub.getPlayerHubDataDAO().save(finalData));
        }

        player.setData(PlayerHubData.class, data);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        for (PotionEffect effect : event.getPlayer().getActivePotionEffects()) {
            event.getPlayer().removePotionEffect(effect.getType());
        }

        NetworkPlayer player = CoreAPI.getPlayer(event.getPlayer());
        if (player != null && player.getRank().ordinal() >= Rank.ULTRA.ordinal()) {
            if (!event.getPlayer().getAllowFlight()) {
                event.getPlayer().setAllowFlight(true);
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        PreferencesMenu.cleanup(event.getPlayer());
    }
}
