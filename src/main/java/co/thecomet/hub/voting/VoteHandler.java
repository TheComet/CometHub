package co.thecomet.hub.voting;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.redis.bukkit.CRBukkit;
import co.thecomet.redis.redis.pubsub.NetTaskSubscribe;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.HashMap;

public class VoteHandler {
    public VoteHandler() {
        CRBukkit.getInstance().getRedis().registerChannel("bukkit-voting");
        CRBukkit.getInstance().getRedis().registerTask(this);
    }

    @NetTaskSubscribe(name = "forwardVote", args = {"serviceName", "username", "address", "timestamp"})
    public void forwardVote(HashMap<String, Object> args) {
        String serviceName = (String) args.get("serviceName");
        String username = (String) args.get("username");
        String address = (String) args.get("address");
        String timestamp = (String) args.get("timestamp");

        new ArrayList<>(Bukkit.getOnlinePlayers()).stream().forEach(player -> {
            if (player.getName().equalsIgnoreCase(username)) {
                NetworkPlayer np = CoreAPI.getPlayer(player);

                if (np != null) {
                    np.setPoints(np.getPoints() + 500);
                }

                MessageFormatter.sendInfoMessage(player, "You have received 500 points for voting!");
            }
        });
    }
}
