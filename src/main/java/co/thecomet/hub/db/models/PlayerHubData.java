package co.thecomet.hub.db.models;

import co.thecomet.core.config.JsonLocation;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.UUID;

@Entity(value = "network_player_hub_data", noClassnameStored = true)
public class PlayerHubData {
    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    public JsonLocation checkpoint;
    public PlayerPreferences preferences = new PlayerPreferences();
    
    public PlayerHubData() {}

    public PlayerHubData(UUID uuid) {
        this.uuid = uuid.toString();
    }
}
