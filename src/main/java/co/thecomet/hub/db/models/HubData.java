package co.thecomet.hub.db.models;

import co.thecomet.core.config.JsonLocation;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

@Entity(value = "network_hub_data", noClassnameStored = true)
public class HubData {
    @Id
    public ObjectId id;
    public JsonLocation spawn;
    public List<JsonLocation> deliveryBoxes = Lists.newArrayList();
}
