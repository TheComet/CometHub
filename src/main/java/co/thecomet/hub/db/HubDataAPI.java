package co.thecomet.hub.db;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.config.JsonLocation;
import co.thecomet.hub.Hub;
import co.thecomet.hub.db.models.HubData;
import co.thecomet.hub.db.models.PlayerHubData;
import co.thecomet.hub.db.models.PlayerPreferences;
import org.bukkit.Bukkit;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.UUID;

public class HubDataAPI {
    public static PlayerHubData getPlayerHubData(UUID uuid) {
        return Hub.getPlayerHubDataDAO().findOne("uuid", uuid.toString());
    }

    public static void updateCheckpoint(UUID uuid, PlayerHubData data) {
        CoreAPI.async(() -> {
            UpdateOperations<PlayerHubData> ops = Hub.getPlayerHubDataDAO().createUpdateOperations();
            ops.set("checkpoint", data.checkpoint);
            Hub.getPlayerHubDataDAO().update(Hub.getPlayerHubDataDAO().createQuery().field("uuid").equal(uuid.toString()), ops);
        });
    }

    public static HubData initHubData() {
        HubData data = Hub.getInstance().getHubDataDAO().find().get();
        if (data == null) {
            data = new HubData();
            data.spawn = new JsonLocation(Bukkit.getWorlds().get(0).getSpawnLocation());
            Hub.getInstance().getHubDataDAO().save(data);
        }
        return data;
    }

    public static void updatePreferences(UUID uuid, PlayerPreferences preferences) {
        CoreAPI.async(() -> {
            UpdateOperations<PlayerHubData> ops = Hub.getPlayerHubDataDAO().createUpdateOperations();
            ops.set("preferences", preferences);
            Hub.getPlayerHubDataDAO().update(Hub.getPlayerHubDataDAO().createQuery().field("uuid").equal(uuid.toString()), ops);
        });
    }

    public static void addDeliveryBox(JsonLocation location) {
        UpdateOperations<HubData> ops = Hub.getHubDataDAO().createUpdateOperations();
        ops.add("deliveryBoxes", location);
        Hub.getHubDataDAO().update(Hub.getHubDataDAO().createQuery(), ops);
    }
}
