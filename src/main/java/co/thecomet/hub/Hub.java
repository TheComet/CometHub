package co.thecomet.hub;

import co.thecomet.core.CorePlugin;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.hub.commands.DebugCommands;
import co.thecomet.hub.db.models.HubData;
import co.thecomet.hub.db.models.PlayerHubData;
import co.thecomet.hub.modules.delivery.data.UserDeliveryStats;
import co.thecomet.hub.status.ServerInfoListener;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.ArrayList;
import java.util.List;

public class Hub extends CorePlugin implements Listener {
    private static Hub instance;
    private ServerInfoListener serverInfoListener;

    @Override
    public void enable() {
        instance = this;
        serverInfoListener = new ServerInfoListener();
        new DebugCommands();

        Bukkit.getWorlds().get(0).setGameRuleValue("doDaylightCycle", String.valueOf(false));
        Bukkit.getWorlds().get(0).setTime(6000);

        ModuleManager.registerModule(new HubModule());
    }

    @Override
    public void disable() {
        //
    }

    @Override
    public String getType() {
        return "HUB";
    }

    @Override
    public List<Class<?>> getDBClasses() {
        return new ArrayList<Class<?>>() {{
            add(HubData.class);
            add(PlayerHubData.class);
            add(UserDeliveryStats.class);
        }};
    }

    public ServerInfoListener getServerInfoListener() {
        return serverInfoListener;
    }

    public static Hub getInstance() {
        return instance;
    }

    public static BasicDAO<HubData, ObjectId> getHubDataDAO() {
        return getDAO(HubData.class);
    }

    public static BasicDAO<PlayerHubData, ObjectId> getPlayerHubDataDAO() {
        return getDAO(PlayerHubData.class);
    }

    public static BasicDAO<UserDeliveryStats, ObjectId> getUserDeliveryStatsDAO() {
        return getDAO(UserDeliveryStats.class);
    }
}
