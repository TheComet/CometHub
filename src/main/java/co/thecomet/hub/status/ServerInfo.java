package co.thecomet.hub.status;

public class ServerInfo {
    private String id, type, status;
    private int playersOnline, maxPlayers;
    private boolean vipOnly, joinable, spectaorJoinInProgress;

    public ServerInfo(String id, String type, String status, int playersOnline, int maxPlayers, boolean vipOnly, boolean joinable, boolean spectaorJoinInProgress) {
        this.id = id;
        this.type = type;
        this.status = status;
        this.playersOnline = playersOnline;
        this.maxPlayers = maxPlayers;

        this.vipOnly = vipOnly;
        this.joinable = joinable;
        this.spectaorJoinInProgress = spectaorJoinInProgress;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public int getPlayersOnline() {
        return playersOnline;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public boolean isVipOnly() {
        return vipOnly;
    }

    public boolean isJoinable() {
        return joinable;
    }

    public boolean isSpectaorJoinInProgress() {
        return spectaorJoinInProgress;
    }
}
