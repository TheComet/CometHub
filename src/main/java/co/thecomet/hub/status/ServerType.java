package co.thecomet.hub.status;

import org.bukkit.Material;

public enum ServerType {
    HUB("Hub", "VIP Hub", "&4&lL&cobbies", Material.MAGMA_CREAM),
    SG("&6&lLucky &8S&7urvival&8G&7ames", "VIP Lucky SG", "&6&lLucky &8SG", Material.IRON_SWORD),
    FACTIONS("Lucky Factions", "VIP Lucky Factions", "Factions", Material.TNT),
    WARGROUNDS("&8W&7ar&8G&7rounds", "VIP Wargrounds", "&8W&7ar&8G&7rounds", Material.BOW),
    SW("&6&lLucky &8S&7ky&8W&7ars", "VIP Lucky SW", "&6&lLucky &8SW", Material.GRASS);

    private String display;
    private String vipDisplay;
    private String inventoryDisplay;
    private Material icon;

    ServerType(String display, String vipDisplay, String inventoryDisplay, Material icon) {
        this.display = display;
        this.vipDisplay = vipDisplay;
        this.inventoryDisplay = inventoryDisplay;
        this.icon = icon;
    }

    public String getDisplay() {
        return display;
    }

    public String getVipDisplay() {
        return vipDisplay;
    }

    public String getInventoryDisplay() {
        return inventoryDisplay;
    }

    public Material getIcon() {
        return icon;
    }

    public static ServerType findMatching(String type) {
        for (ServerType t : values()) {
            if (type.equalsIgnoreCase(t.name())) {
                return t;
            }
        }

        return null;
    }
}
